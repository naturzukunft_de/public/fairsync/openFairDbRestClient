/*
 * OpenFairDB API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 0.9.3
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package org.openapitools.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.openapitools.client.model.ContactEmail;
import org.openapitools.client.model.ContactPhone;
import org.openapitools.client.model.CreatedAt;
import org.openapitools.client.model.EventTime;
import org.openapitools.client.model.ImageLink;
import org.openapitools.client.model.ImageUrl;

/**
 * Event
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen", date = "2020-12-19T15:07:54.107442+01:00[Europe/Rome]")
public class Event {
  public static final String SERIALIZED_NAME_ID = "id";
  @SerializedName(SERIALIZED_NAME_ID)
  private String id;

  public static final String SERIALIZED_NAME_TITLE = "title";
  @SerializedName(SERIALIZED_NAME_TITLE)
  private String title;

  public static final String SERIALIZED_NAME_DESCRIPTION = "description";
  @SerializedName(SERIALIZED_NAME_DESCRIPTION)
  private String description;

  public static final String SERIALIZED_NAME_START = "start";
  @SerializedName(SERIALIZED_NAME_START)
  private EventTime start = null;

  public static final String SERIALIZED_NAME_END = "end";
  @SerializedName(SERIALIZED_NAME_END)
  private EventTime end = null;

  public static final String SERIALIZED_NAME_CREATED_AT = "created_at";
  @SerializedName(SERIALIZED_NAME_CREATED_AT)
  private CreatedAt createdAt = null;

  public static final String SERIALIZED_NAME_CREATED_BY = "created_by";
  @SerializedName(SERIALIZED_NAME_CREATED_BY)
  private String createdBy;

  public static final String SERIALIZED_NAME_LAT = "lat";
  @SerializedName(SERIALIZED_NAME_LAT)
  private Double lat;

  public static final String SERIALIZED_NAME_LNG = "lng";
  @SerializedName(SERIALIZED_NAME_LNG)
  private Double lng;

  public static final String SERIALIZED_NAME_STREET = "street";
  @SerializedName(SERIALIZED_NAME_STREET)
  private String street;

  public static final String SERIALIZED_NAME_ZIP = "zip";
  @SerializedName(SERIALIZED_NAME_ZIP)
  private String zip;

  public static final String SERIALIZED_NAME_CITY = "city";
  @SerializedName(SERIALIZED_NAME_CITY)
  private String city;

  public static final String SERIALIZED_NAME_COUNTRY = "country";
  @SerializedName(SERIALIZED_NAME_COUNTRY)
  private String country;

  public static final String SERIALIZED_NAME_STATE = "state";
  @SerializedName(SERIALIZED_NAME_STATE)
  private String state;

  public static final String SERIALIZED_NAME_EMAIL = "email";
  @SerializedName(SERIALIZED_NAME_EMAIL)
  private ContactEmail email = null;

  public static final String SERIALIZED_NAME_TELEPHONE = "telephone";
  @SerializedName(SERIALIZED_NAME_TELEPHONE)
  private ContactPhone telephone = null;

  public static final String SERIALIZED_NAME_TAGS = "tags";
  @SerializedName(SERIALIZED_NAME_TAGS)
  private List<String> tags = null;

  public static final String SERIALIZED_NAME_HOMEPAGE = "homepage";
  @SerializedName(SERIALIZED_NAME_HOMEPAGE)
  private String homepage;

  /**
   * Type of registration
   */
  @JsonAdapter(RegistrationEnum.Adapter.class)
  public enum RegistrationEnum {
    EMAIL("email"),
    
    TELEPHONE("telephone"),
    
    HOMEPAGE("homepage");

    private String value;

    RegistrationEnum(String value) {
      this.value = value;
    }

    public String getValue() {
      return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }

    public static RegistrationEnum fromValue(String value) {
      for (RegistrationEnum b : RegistrationEnum.values()) {
        if (b.value.equals(value)) {
          return b;
        }
      }
      throw new IllegalArgumentException("Unexpected value '" + value + "'");
    }

    public static class Adapter extends TypeAdapter<RegistrationEnum> {
      @Override
      public void write(final JsonWriter jsonWriter, final RegistrationEnum enumeration) throws IOException {
        jsonWriter.value(enumeration.getValue());
      }

      @Override
      public RegistrationEnum read(final JsonReader jsonReader) throws IOException {
        String value =  jsonReader.nextString();
        return RegistrationEnum.fromValue(value);
      }
    }
  }

  public static final String SERIALIZED_NAME_REGISTRATION = "registration";
  @SerializedName(SERIALIZED_NAME_REGISTRATION)
  private RegistrationEnum registration;

  public static final String SERIALIZED_NAME_ORGANIZER = "organizer";
  @SerializedName(SERIALIZED_NAME_ORGANIZER)
  private String organizer;

  public static final String SERIALIZED_NAME_IMAGE_URL = "image_url";
  @SerializedName(SERIALIZED_NAME_IMAGE_URL)
  private ImageUrl imageUrl = null;

  public static final String SERIALIZED_NAME_IMAGE_LINK_URL = "image_link_url";
  @SerializedName(SERIALIZED_NAME_IMAGE_LINK_URL)
  private ImageLink imageLinkUrl = null;


  public Event id(String id) {
    
    this.id = id;
    return this;
  }

   /**
   * Identifier of a resource 
   * @return id
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(example = "7cee99c287094a94acbdcf29ffff2e85", value = "Identifier of a resource ")

  public String getId() {
    return id;
  }


  public void setId(String id) {
    this.id = id;
  }


  public Event title(String title) {
    
    this.title = title;
    return this;
  }

   /**
   * Get title
   * @return title
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(example = "A great event", value = "")

  public String getTitle() {
    return title;
  }


  public void setTitle(String title) {
    this.title = title;
  }


  public Event description(String description) {
    
    this.description = description;
    return this;
  }

   /**
   * Get description
   * @return description
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(example = "Detailed description of the event", value = "")

  public String getDescription() {
    return description;
  }


  public void setDescription(String description) {
    this.description = description;
  }


  public Event start(EventTime start) {
    
    this.start = start;
    return this;
  }

   /**
   * Get start
   * @return start
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public EventTime getStart() {
    return start;
  }


  public void setStart(EventTime start) {
    this.start = start;
  }


  public Event end(EventTime end) {
    
    this.end = end;
    return this;
  }

   /**
   * Get end
   * @return end
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public EventTime getEnd() {
    return end;
  }


  public void setEnd(EventTime end) {
    this.end = end;
  }


  public Event createdAt(CreatedAt createdAt) {
    
    this.createdAt = createdAt;
    return this;
  }

   /**
   * Get createdAt
   * @return createdAt
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public CreatedAt getCreatedAt() {
    return createdAt;
  }


  public void setCreatedAt(CreatedAt createdAt) {
    this.createdAt = createdAt;
  }


  public Event createdBy(String createdBy) {
    
    this.createdBy = createdBy;
    return this;
  }

   /**
   * The email address of the user who is responsible for the content. This information is only available for authorized organizations. 
   * @return createdBy
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "The email address of the user who is responsible for the content. This information is only available for authorized organizations. ")

  public String getCreatedBy() {
    return createdBy;
  }


  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }


  public Event lat(Double lat) {
    
    this.lat = lat;
    return this;
  }

   /**
   * Geographic latitude (in degrees)
   * minimum: -90.0
   * maximum: 90.0
   * @return lat
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(example = "37.2", value = "Geographic latitude (in degrees)")

  public Double getLat() {
    return lat;
  }


  public void setLat(Double lat) {
    this.lat = lat;
  }


  public Event lng(Double lng) {
    
    this.lng = lng;
    return this;
  }

   /**
   * Geographic longitude (in degrees)
   * minimum: -180.0
   * maximum: 180.0
   * @return lng
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(example = "120.7", value = "Geographic longitude (in degrees)")

  public Double getLng() {
    return lng;
  }


  public void setLng(Double lng) {
    this.lng = lng;
  }


  public Event street(String street) {
    
    this.street = street;
    return this;
  }

   /**
   * Get street
   * @return street
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getStreet() {
    return street;
  }


  public void setStreet(String street) {
    this.street = street;
  }


  public Event zip(String zip) {
    
    this.zip = zip;
    return this;
  }

   /**
   * Get zip
   * @return zip
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getZip() {
    return zip;
  }


  public void setZip(String zip) {
    this.zip = zip;
  }


  public Event city(String city) {
    
    this.city = city;
    return this;
  }

   /**
   * Get city
   * @return city
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getCity() {
    return city;
  }


  public void setCity(String city) {
    this.city = city;
  }


  public Event country(String country) {
    
    this.country = country;
    return this;
  }

   /**
   * Get country
   * @return country
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getCountry() {
    return country;
  }


  public void setCountry(String country) {
    this.country = country;
  }


  public Event state(String state) {
    
    this.state = state;
    return this;
  }

   /**
   * Get state
   * @return state
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getState() {
    return state;
  }


  public void setState(String state) {
    this.state = state;
  }


  public Event email(ContactEmail email) {
    
    this.email = email;
    return this;
  }

   /**
   * Get email
   * @return email
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public ContactEmail getEmail() {
    return email;
  }


  public void setEmail(ContactEmail email) {
    this.email = email;
  }


  public Event telephone(ContactPhone telephone) {
    
    this.telephone = telephone;
    return this;
  }

   /**
   * Get telephone
   * @return telephone
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public ContactPhone getTelephone() {
    return telephone;
  }


  public void setTelephone(ContactPhone telephone) {
    this.telephone = telephone;
  }


  public Event tags(List<String> tags) {
    
    this.tags = tags;
    return this;
  }

  public Event addTagsItem(String tagsItem) {
    if (this.tags == null) {
      this.tags = new ArrayList<String>();
    }
    this.tags.add(tagsItem);
    return this;
  }

   /**
   * Get tags
   * @return tags
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(example = "[\"organic\",\"non-profit\"]", value = "")

  public List<String> getTags() {
    return tags;
  }


  public void setTags(List<String> tags) {
    this.tags = tags;
  }


  public Event homepage(String homepage) {
    
    this.homepage = homepage;
    return this;
  }

   /**
   * Get homepage
   * @return homepage
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(example = "https://www.slowtec.de/", value = "")

  public String getHomepage() {
    return homepage;
  }


  public void setHomepage(String homepage) {
    this.homepage = homepage;
  }


  public Event registration(RegistrationEnum registration) {
    
    this.registration = registration;
    return this;
  }

   /**
   * Type of registration
   * @return registration
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(example = "telephone", value = "Type of registration")

  public RegistrationEnum getRegistration() {
    return registration;
  }


  public void setRegistration(RegistrationEnum registration) {
    this.registration = registration;
  }


  public Event organizer(String organizer) {
    
    this.organizer = organizer;
    return this;
  }

   /**
   * Get organizer
   * @return organizer
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getOrganizer() {
    return organizer;
  }


  public void setOrganizer(String organizer) {
    this.organizer = organizer;
  }


  public Event imageUrl(ImageUrl imageUrl) {
    
    this.imageUrl = imageUrl;
    return this;
  }

   /**
   * Get imageUrl
   * @return imageUrl
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public ImageUrl getImageUrl() {
    return imageUrl;
  }


  public void setImageUrl(ImageUrl imageUrl) {
    this.imageUrl = imageUrl;
  }


  public Event imageLinkUrl(ImageLink imageLinkUrl) {
    
    this.imageLinkUrl = imageLinkUrl;
    return this;
  }

   /**
   * Get imageLinkUrl
   * @return imageLinkUrl
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public ImageLink getImageLinkUrl() {
    return imageLinkUrl;
  }


  public void setImageLinkUrl(ImageLink imageLinkUrl) {
    this.imageLinkUrl = imageLinkUrl;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Event event = (Event) o;
    return Objects.equals(this.id, event.id) &&
        Objects.equals(this.title, event.title) &&
        Objects.equals(this.description, event.description) &&
        Objects.equals(this.start, event.start) &&
        Objects.equals(this.end, event.end) &&
        Objects.equals(this.createdAt, event.createdAt) &&
        Objects.equals(this.createdBy, event.createdBy) &&
        Objects.equals(this.lat, event.lat) &&
        Objects.equals(this.lng, event.lng) &&
        Objects.equals(this.street, event.street) &&
        Objects.equals(this.zip, event.zip) &&
        Objects.equals(this.city, event.city) &&
        Objects.equals(this.country, event.country) &&
        Objects.equals(this.state, event.state) &&
        Objects.equals(this.email, event.email) &&
        Objects.equals(this.telephone, event.telephone) &&
        Objects.equals(this.tags, event.tags) &&
        Objects.equals(this.homepage, event.homepage) &&
        Objects.equals(this.registration, event.registration) &&
        Objects.equals(this.organizer, event.organizer) &&
        Objects.equals(this.imageUrl, event.imageUrl) &&
        Objects.equals(this.imageLinkUrl, event.imageLinkUrl);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, title, description, start, end, createdAt, createdBy, lat, lng, street, zip, city, country, state, email, telephone, tags, homepage, registration, organizer, imageUrl, imageLinkUrl);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Event {\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    title: ").append(toIndentedString(title)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("    start: ").append(toIndentedString(start)).append("\n");
    sb.append("    end: ").append(toIndentedString(end)).append("\n");
    sb.append("    createdAt: ").append(toIndentedString(createdAt)).append("\n");
    sb.append("    createdBy: ").append(toIndentedString(createdBy)).append("\n");
    sb.append("    lat: ").append(toIndentedString(lat)).append("\n");
    sb.append("    lng: ").append(toIndentedString(lng)).append("\n");
    sb.append("    street: ").append(toIndentedString(street)).append("\n");
    sb.append("    zip: ").append(toIndentedString(zip)).append("\n");
    sb.append("    city: ").append(toIndentedString(city)).append("\n");
    sb.append("    country: ").append(toIndentedString(country)).append("\n");
    sb.append("    state: ").append(toIndentedString(state)).append("\n");
    sb.append("    email: ").append(toIndentedString(email)).append("\n");
    sb.append("    telephone: ").append(toIndentedString(telephone)).append("\n");
    sb.append("    tags: ").append(toIndentedString(tags)).append("\n");
    sb.append("    homepage: ").append(toIndentedString(homepage)).append("\n");
    sb.append("    registration: ").append(toIndentedString(registration)).append("\n");
    sb.append("    organizer: ").append(toIndentedString(organizer)).append("\n");
    sb.append("    imageUrl: ").append(toIndentedString(imageUrl)).append("\n");
    sb.append("    imageLinkUrl: ").append(toIndentedString(imageLinkUrl)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

